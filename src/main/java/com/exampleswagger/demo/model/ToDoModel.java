package com.exampleswagger.demo.model;

import lombok.Data;

@Data
public class ToDoModel {

    private String task;
    private Integer id;
    private Status status;

    public void setStatus(String status) {
        if(status.equals("active")) this.status = Status.active;
        if(status.equals("inprogress")) this.status = Status.inprogress;
        if (status.equals("close")) this.status = Status.close;
    }

    private enum Status{
        inprogress,
        close,
        active
    }
}

