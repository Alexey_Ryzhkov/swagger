package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.repository.ToDoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ToDoService {

    private final ToDoRepository toDoRepository;

    public ToDoModel todo(int id,String task,String status){
        var todo = new ToDoModel();
        todo.setId(id);
        todo.setStatus(status);
        todo.setTask(task);
        return todo;
    }

    public void add(ToDoModel model){
        toDoRepository.add(model);
    }

    public List<ToDoModel> getList(){
        return toDoRepository.getList();
    }
}
