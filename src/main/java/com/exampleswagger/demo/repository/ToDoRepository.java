package com.exampleswagger.demo.repository;

import com.exampleswagger.demo.model.ToDoModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ToDoRepository {

    private List<ToDoModel> storage = new ArrayList<>();

    public void add (ToDoModel model){
        storage.add(model);
    }

    public List<ToDoModel> getList() {
        return storage;
    }
}
