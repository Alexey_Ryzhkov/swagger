package com.exampleswagger.demo.controller;

import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.service.ToDoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ToDoController {

    private final ToDoService toDoService;

    @GetMapping("/addToDo")
    public void add(ToDoModel model){
        toDoService.add(model);
    }

    @GetMapping("/todoList")
    public List<ToDoModel> getList(){
        return toDoService.getList();
    }
}
